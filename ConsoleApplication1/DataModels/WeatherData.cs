﻿using System;
using System.Collections.Generic;
using System.Xml.Linq;

namespace ConsoleApplication1.DataModels
{
    public class WeatherData
    {

        public WeatherData()
        {
            // Initialize list to prevent null exceptions in foreach loops
            TemperatureForecasts = new List<Forecast>();
            LastModified = DateTime.Now.ToString("dd.MM.yyyy HH:mm:ss");
        }

        public string LastModified { get; set; }
        public WeatherStatus Status { get; set; }
        public XDocument Data { get; set; }
        public string ErrorMessage { get; set; }
        public string LocationName { get; set; }
        public string Latitude { get; set; }
        public string Longnitude { get; set; }
        public List<Forecast> TemperatureForecasts { get; set; }
    }

    public enum WeatherStatus {
        DataFetched,
        UnableToFetchData,
        BadXmlFormat,
        DataAnalysed
    }

}
