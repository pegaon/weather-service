﻿using System;

namespace ConsoleApplication1.DataModels
{
    public class Forecast
    {
        public DateTime TimeUTC { get; set; }
        public int Temperature { get; set; }
        public bool MinAlert { get; set; }
        public bool MaksAlert { get; set; }
    }
}
