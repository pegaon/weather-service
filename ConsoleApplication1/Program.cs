﻿using ConsoleApplication1.Services;
using System.Xml.Linq;
using ConsoleApplication1.DataModels;
using System;
using System.Collections.Generic;
using System.Configuration;

namespace ConsoleApplication1
{
    class Program
    {
        static void Main(string[] args)
        {
            GetWeatherForecast();

            var interval = Convert.ToInt32(ConfigurationManager.AppSettings["timerinterval"].ToString());

            System.Timers.Timer timer = new System.Timers.Timer();
            timer.Interval = 60000 * interval; // Interval in minutes
            timer.Elapsed += new System.Timers.ElapsedEventHandler(OnTimer);
            timer.Start();

            Console.ReadLine();
        }

        public static void OnTimer(object sender, System.Timers.ElapsedEventArgs args)
        {
            GetWeatherForecast();
            Console.WriteLine("Timer elapsed " + DateTime.Now.ToString("dd.MM.yyyy HH:mm:ss"));
        }

        private static void GetWeatherForecast()
        {
            var httpService = new HttpService();
            var analysingService = new AnalysingService();
            var htmlService = new HtmlService();
            var alertService = new AlertService();

            // 1. Get locations and their max and min alert temperatures
            var locationXml = XDocument.Load("Locations/Locations.xml");
            var locations = locationXml.Descendants("location");

            var weatherDatas = new List<WeatherData>();

            foreach (var loc in locations)
            {
                var lat = loc.Element("latitude").Value;
                var lon = loc.Element("longnitude").Value;
                var minAlert = Convert.ToInt32(loc.Element("minalert").Value);
                var maxalert = Convert.ToInt32(loc.Element("maxalert").Value);

                var weatherData = httpService.GetWeatherData(lat, lon);

                if (weatherData.Status == WeatherStatus.DataFetched)
                {
                    weatherData = analysingService.AnalyseWeatherData(weatherData, minAlert, maxalert);
                }

                weatherDatas.Add(weatherData);
            }

            // Log results to Log.html
            htmlService.LogWeatherServiceResults(weatherDatas);

            alertService.NotifyAlertsToUsers(weatherDatas);
        }
    }
}
