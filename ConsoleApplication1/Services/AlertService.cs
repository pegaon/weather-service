﻿using ConsoleApplication1.DataModels;
using ConsoleApplication1.Interfaces;
using System.Collections.Generic;

namespace ConsoleApplication1.Services
{
    public class AlertService : IAlertService
    {
        public void NotifyAlertsToUsers(List<WeatherData> weatherdatas)
        {
            // Todo: create logic that sends email notifications about temperature alerts to recipients. 
            // Recipient can be added to Locations.xml
        }
    }
}
