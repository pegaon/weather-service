﻿using ConsoleApplication1.DataModels;
using ConsoleApplication1.Interfaces;
using System.Text;
using System.Linq;
using System.IO;
using System.Collections.Generic;
using System.Configuration;

namespace ConsoleApplication1.Services
{
    public class HtmlService : IHtmlService
    {
        public void LogWeatherServiceResults(List<WeatherData> weatherdatas)
        {
            // 1. Create html common tags
            var builder = new StringBuilder();
            builder.AppendLine("<!DOCTYPE html>");
            builder.AppendLine("<html lang=\"en\" xmlns=\"http://www.w3.org/1999/xhtml\">");
            builder.AppendLine("<head>");
            builder.AppendLine("<meta charset=\"utf-8\" />");
            builder.AppendLine("<title>Weather log</title>");
            builder.AppendLine("</head>");
            builder.AppendLine("<body>");

            // 2. Log weather results
            foreach (var weatherdata in weatherdatas)
            {
                // 3. Log location name
                builder.AppendLine(weatherdata.LastModified);
                builder.AppendLine("<h1>Weather log</h1>");
                builder.AppendLine("<h2>" + weatherdata.LocationName + "</h2>");

                // 4. Log alerts
                if (weatherdata.TemperatureForecasts.Any(x => x.MinAlert)) builder.AppendLine("<b>Minimum temperature alert!</b>");
                if (weatherdata.TemperatureForecasts.Any(x => x.MaksAlert)) builder.AppendLine("<b>Maximum temperature alert!</b>");
                if (!weatherdata.TemperatureForecasts.Any(x => x.MaksAlert) && !weatherdata.TemperatureForecasts.Any(x => x.MinAlert)) builder.AppendLine("<p>No alerts.</p>");

                // 5. Log coordinates
                builder.AppendLine("<p>Latitude: " + weatherdata.Latitude + " Longnitude: " + weatherdata.Longnitude + "</p>");

                // 6. Log network errors
                if (weatherdata.Status == WeatherStatus.UnableToFetchData)
                {
                    builder.AppendLine("<b>Service wasn't able to fetch data</b>");
                }

                // 7. Log xml format errors
                if (weatherdata.Status == WeatherStatus.BadXmlFormat)
                {
                    builder.AppendLine("<b>Service wasn't able to analyse data</b>");
                }

                // 8. Log time and temperature
                builder.AppendLine("<ul>");

                foreach (var f in weatherdata.TemperatureForecasts)
                {
                    if (f.MinAlert || f.MaksAlert)
                    {
                        builder.AppendLine("<li><b>" + f.TimeUTC.ToLocalTime().ToString("dd.MM.yyyy HH:mm") + "&nbsp;&nbsp;&nbsp;" + f.Temperature.ToString() + "</b></li>");
                    }
                    else
                    {
                        builder.AppendLine("<li>" + f.TimeUTC.ToLocalTime().ToString("dd.MM.yyyy HH:mm") + "&nbsp;&nbsp;&nbsp;" + f.Temperature.ToString() + "</li>");
                    }
                }

                builder.AppendLine("</ul>");
            }

            // 9. Close common html tags
            builder.AppendLine("</body>");
            builder.AppendLine("</html>");


            var logFilePath = ConfigurationManager.AppSettings["logfilepath"].ToString();

            try
            {
                // 10. Save file. Change filepath in app.config to match location from where web server can serve it
                File.WriteAllText(logFilePath, builder.ToString());
            }
            catch (System.Exception ex)
            {
                //Todo: Add error logging
            }

        }
    }
}
