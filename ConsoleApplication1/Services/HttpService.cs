﻿using ConsoleApplication1.DataModels;
using ConsoleApplication1.Interfaces;
using System;
using System.Configuration;
using System.Xml.Linq;

namespace ConsoleApplication1.Services
{
    public class HttpService : IHttpService
    {
        public WeatherData GetWeatherData(string latitude, string longnitude)
        {
            // Example api url http://api.openweathermap.org/data/2.5/forecast?lat=60.2087898&lon=25.1415427&APPID=7bd1ed11afd5f39f8e21ab84afe061b4&mode=xml

            // 1. Create api url
            var urlFormat = ConfigurationManager.AppSettings["weatherurl"].ToString();
            var url = string.Format(urlFormat, latitude, longnitude);

            var weatherData = new WeatherData();

            try
            {
                // 2. Fetch weather data
                weatherData.Data = XDocument.Load(url);
                weatherData.Status = WeatherStatus.DataFetched;
                return weatherData;
            }
            catch (Exception ex)
            {
                //Todo: Add error logging
                weatherData.Status = WeatherStatus.UnableToFetchData;
                weatherData.ErrorMessage = ex.Message;
                return weatherData;
            }
          
        }
    }
}
