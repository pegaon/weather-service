﻿using ConsoleApplication1.DataModels;
using ConsoleApplication1.Interfaces;
using System;
using System.Linq;

namespace ConsoleApplication1.Services
{
    public class AnalysingService : IAnalysingService
    {
        public WeatherData AnalyseWeatherData(WeatherData weatherData, double minAlert, double maxAlert)
        {
            try
            {
                // 1. Get location information
                var location = weatherData.Data.Descendants("location").First();
                weatherData.LocationName = location.Element("name").Value;
                weatherData.Latitude = location.Element("location").Attribute("latitude").Value;
                weatherData.Longnitude = location.Element("location").Attribute("longitude").Value;

                // 2. Get temperature forecasts
                var forecasts = weatherData.Data.Descendants("forecast").First().Descendants("time");

                foreach (var f in forecasts)
                {
                    var forecast = new Forecast();

                    // 2 Get time and temperature
                    var time = f.Attribute("from").Value;
                    var temperature = f.Element("temperature").Attribute("value").Value;

                    // 3. Parse time. Time in UTC
                    DateTime dateTime;
                    if (DateTime.TryParse(time, out dateTime)) forecast.TimeUTC = dateTime;

                    // 4. Parse temperature and check alerts
                    double tempe;
                    if (double.TryParse(temperature, out tempe))
                    {
                        forecast.Temperature = Convert.ToInt32(tempe);
                        forecast.MaksAlert = forecast.Temperature > maxAlert;
                        forecast.MinAlert = forecast.Temperature < minAlert;
                    }

                    weatherData.TemperatureForecasts.Add(forecast);
                }

                weatherData.Status = WeatherStatus.DataAnalysed;
                return weatherData;
            }
            catch (Exception ex)
            {
                //Todo: Add error logging
                weatherData.Status = WeatherStatus.BadXmlFormat;
                weatherData.ErrorMessage = ex.Message;
                return weatherData;
            }
        }
    }
}
