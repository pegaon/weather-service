﻿using ConsoleApplication1.DataModels;
using System.Collections.Generic;

namespace ConsoleApplication1.Interfaces
{
    public interface IAlertService
    {
        void NotifyAlertsToUsers(List<WeatherData> weatherdatas);
    }
}
