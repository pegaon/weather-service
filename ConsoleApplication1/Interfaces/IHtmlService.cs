﻿using ConsoleApplication1.DataModels;
using System.Collections.Generic;

namespace ConsoleApplication1.Interfaces
{
    public interface IHtmlService
    {
        void LogWeatherServiceResults(List<WeatherData> weatherdatas);
    }
}
