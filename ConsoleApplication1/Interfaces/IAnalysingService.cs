﻿using ConsoleApplication1.DataModels;

namespace ConsoleApplication1.Interfaces
{
    public interface IAnalysingService
    {
        WeatherData AnalyseWeatherData(WeatherData weatherData, double minAlert, double maxAlert);
    }
}
