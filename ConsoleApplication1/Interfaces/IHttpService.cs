﻿using ConsoleApplication1.DataModels;

namespace ConsoleApplication1.Interfaces
{
    public interface IHttpService
    {
        WeatherData GetWeatherData(string latitude, string longnitude);
    }
}
